# # Prerequisites
# sudo apt update && apt install -y \
#   curl \
#   git \
#   unzip \
#   xz-utils \
#   zip \
#   libglu1-mesa \
#   openjdk-8-jdk \
#   wget

# # Set up new user
# sudo useradd -ms /bin/bash guitou
# # USER guitou
# # WORKDIR /home/guitou/

# # Prepare Android directories and system variables
# mkdir -p /home/guitou/tools/Android/sdk
# ENV 
# export ANDROID_SDK_ROOT=/home/guitou/tools/Android/sdk
# mkdir -p /home/guitou/.android && touch /home/guitou/.android/repositories.cfg

# Set up Android SDK
# wget -O sdk-tools.zip https://dl.google.com/android/repository/sdk-tools-linux-4333796.zip
# unzip sdk-tools.zip && rm sdk-tools.zip
# mv tools /home/guitou/tools/Android/sdk/tools
# cd /home/guitou/tools/Android/sdk/tools/bin && yes | ./sdkmanager --licenses
# cd /home/guitou/tools/Android/sdk/tools/bin && ./sdkmanager "build-tools;29.0.2" "patcher;v4" "platform-tools" "platforms;android-29" "sources;android-29"
# export PATH="$PATH:/home/guitou/tools/Android/sdk/platform-tools"

# Download Flutter SDK
# git clone https://github.com/flutter/flutter.git /home/guitou/tools/flutter
# export PATH="$PATH:/home/guitou/tools/flutter/bin"
# flutter channel dev
# flutter upgrade
# flutter doctor

# Store credentials
# echo "#!/bin/bash" > /home/guitou/credential-helper.sh
# echo "echo username=\$GIT_AUTH_USERNAME" >> /home/guitou/credential-helper.sh
# echo "echo password=\$GIT_AUTH_PASSWORD" >> /home/guitou/credential-helper.sh

# cat /home/guitou/credential-helper.sh

# git config --global credential.helper "/bin/bash /home/guitou/credential-helper.sh"

# git clone $GIT_REPO_URL /home/guitou/tools/boilerplate

# cd /home/guitou/tools/boilerplate && ls -l && flutter build apk
