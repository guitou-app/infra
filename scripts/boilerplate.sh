echo "#!/bin/bash" > /home/guitou/credential-helper.sh
echo "echo username=\$GIT_AUTH_USERNAME" >> /home/guitou/credential-helper.sh
echo "echo password=\$GIT_AUTH_PASSWORD" >> /home/guitou/credential-helper.sh
cat /home/guitou/credential-helper.sh

git config --global credential.helper "/bin/bash /home/guitou/credential-helper.sh"

BOILERPLATE_DIR="/home/guitou/tools/boilerplate"
if [[ -d $BOILERPLATE_DIR ]]
then
  rm -rf $BOILERPLATE_DIR
fi
git clone $GIT_BOILERPLATE_URL $BOILERPLATE_DIR

if [[ $? -ne 0 ]]
then
  exit 1
fi

if [[ ! -d $BOILERPLATE_DIR ]]
then
  exit 1
fi
cd $BOILERPLATE_DIR
ls -la
flutter build apk
