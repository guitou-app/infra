openssl req -x509 -newkey rsa:4096 -sha256 -nodes -keyout tls-app.key -out tls-app.cert -subj "/CN=app.guitou.local"
kubectl create secret tls guitou.api.local.tls --cert=tls-api.crt --key=tls-api.key

# https://deliciousbrains.com/ssl-certificate-authority-for-local-https-development/
# https://www.freecodecamp.org/news/how-to-get-https-working-on-your-local-development-environment-in-5-minutes-7af615770eec/

# Generate CA
openssl genrsa -des3 -out guitou-rootCA.key 2048 # guitou as passphrase
openssl req -x509 -new -nodes -key guitou-rootCA.key -sha256 -days 1024 -out guitou-rootCA.pem

# Generate certificates for www.guitou.local

# CONTENT OF www.guitou.local.cnf
#
# [req]
# default_bits = 2048
# prompt = no
# default_md = sha256
# distinguished_name = dn

# [dn]
# C=US
# ST=RandomState
# L=Yaounde
# O=IT
# OU=IT
# emailAddress=mael.fosso@guitou.cm
# CN = www.guitou.local

# 
# CONTENT OF www.guitou.local.ext
# authorityKeyIdentifier=keyid,issuer
# basicConstraints=CA:FALSE
# keyUsage = digitalSignature, nonRepudiation, keyEncipherment, dataEncipherment
# subjectAltName = @alt_names

# [alt_names]
# DNS.1 = www.guitou.local
# DNS.2 = guitou.local

# Generate Private key and CSR
openssl req -new -sha256 -nodes -out www.guitou.local.csr -newkey rsa:2048 -keyout www.guitou.local.key -config <( cat www.guitou.local.cnf )
# Generate Public key or Certificate
openssl x509 -req -in www.guitou.local.csr -CA guitou-rootCA.crt -CAkey guitou-rootCA.key -CAcreateserial -out www.guitou.local.crt -days 500 -sha256 -extfile www.guitou.local.ext
