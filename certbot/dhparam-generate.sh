#!/bin/bash

if [ "$EUID" -ne 0 ]
then
  echo "Please run as root"
  exit
fi

DOMAIN=company.com
SUBDOMAINS=("" app api)

for sub in "${SUBDOMAINS[@]}"
do
  website=$DOMAIN
  if [ -n "$sub" ]
  then
    website=$sub.$DOMAIN
  fi

  echo $'\n[*]\tGENERATING DHParam for --- ' $website $'\n'

  openssl dhparam -out /etc/ssl/certs/"$website".dhparam-4096.pem 4096
done
