#!/bin/bash

if [ "$EUID" -ne 0 ]
then
  echo "Please run as root"
  exit
fi

DOMAIN=guitou.cm
SUBDOMAINS=("" app api)

for sub in "${SUBDOMAINS[@]}"
do
  website=$DOMAIN
  if [ -n "$sub" ]
  then
    website=$sub.$DOMAIN
  fi

  echo $'\n[*]\tGENERATING Certificate and DHParam for --- ' $website $'\n'
  
  docker container run -it --rm --name certbot \
          -v /etc/letsencrypt/:/etc/letsencrypt \
          -v /var/lib/letsencrypt/:/var/lib/letsencrypt \
          -v /var/www/"$website":/var/www/"$website" \
      certbot/certbot certonly \
          --email mael.fosso@guitou.cm \
          --agree-tos \
          --no-eff-email \
          --webroot -w /var/www/"$website" --force-renewal -d "$website" $(if [[ -z "$sub" ]]; then printf %s -d www.$website; fi)

done
