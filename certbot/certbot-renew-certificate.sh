#!/bin/bash

if [ "$EUID" -ne 0 ]
then
  echo "Please run as root"
  exit
fi

DOMAIN=guitou.cm
SUBDOMAINS=("" app api)

for sub in "${SUBDOMAINS[@]}"
do
  website=$DOMAIN
  if [ -n "$sub" ]
  then
    website=$sub.$DOMAIN
  fi

  echo $'\n[*]\tGENERATING Certificate for --- ' $website $'\n'
  
  docker container run -it --rm --name certbot \
          -v /etc/letsencrypt/:/etc/letsencrypt \
          -v /var/lib/letsencrypt/:/var/lib/letsencrypt \
          -v /var/www/"$website":/var/www/"$website" \
      certbot/certbot renew 

done

docker service update --force nginx 
